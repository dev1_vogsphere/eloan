<?php
/* require the user as the parameter 
PHP ODATA service Reference:
https://davidwalsh.name/web-service-php-mysql-xml-json

GET ALL the PaymentSchedules
04.11.2017
Parameter : status =
APPR - which means Approval Loans
SET  - Which means Settled Loans
CAN  - which means Cancelled Loans
REJ  - Which means Rejected Loans

format = 
xml  - Which means xml format.
json - Which means json format. 
*/

// -- Total Income
$tot_outstanding = 0.00;
$tot_paid = 0.00;
$tot_repayment = 0.00;
require 'database.php';

$found 	= false;
$UseApplicationID = false;
$xml 	= "";
$status = "";
$ApplicationId = null;
$format = "";

// -- $RemainingTerm
if ( !empty($_GET['status'])) 
{
		$status = $_GET['status'];
		$found = true;
}

// -- Query with ApplicationId.
if ( !empty($_GET['ApplicationId'])) 
{
		$ApplicationId = $_GET['ApplicationId'];
		$UseApplicationID = true;
}

/* soak in the passed variable or set our own */			
if(!empty($_GET['format']))		
{
	$format = strtolower($_GET['format']) == 'json' ? 'json' : 'xml'; //xml is the default			
}
else
{
	$format = 'xml';
}

try
 {
	// -- BOC 31.10.2017 --------- //
		 $pdo = Database::connect();
		 $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION); 
// ----------------------------------------------------------------------------------- //
// 					IMS Amount to be paid and Still to be paid.						   //
// ----------------------------------------------------------------------------------- //				 
		// -- Get Payment Invoices from common Tab Data.
		 if($found)
		 {			 
			$sql =  "SELECT * FROM paymentschedule as A join loanapp as B on A.ApplicationId = B.ApplicationId Where ExecApproval = ?";
			$q = $pdo->prepare($sql);
			$q->execute(array($status));
		 }	
		 else if($UseApplicationID)
		 {
			 $sql =  "SELECT * FROM paymentschedule as A join loanapp as B on A.ApplicationId = B.ApplicationId where A.ApplicationId = ?";
			 $q = $pdo->prepare($sql);
			 $q->execute(array($ApplicationId));
		 }
		else
		{
			$sql =  "SELECT * FROM paymentschedule as A join loanapp as B on A.ApplicationId = B.ApplicationId";
			$q = $pdo->prepare($sql);
			$q->execute();
		}	
			   $dataPaymentSchedules = $q->fetchAll(PDO::FETCH_ASSOC); 
			   $xml = '<paymentschedules>';

			   foreach($dataPaymentSchedules as $rowPaymentSchedule)
			   {					  
				// For each payment schedule related to the loan application.
					  $xml = $xml.'<paymentschedule>';
					  $xml = $xml.'<id>'.$rowPaymentSchedule['CustomerId'].'</id>';
					  $xml = $xml.'<applicationId>'.$rowPaymentSchedule['ApplicationId'].'</applicationId>';
					  $xml = $xml.'<item>'.$rowPaymentSchedule['item']. '</item>';
					  $xml = $xml.'<scheduleddate>'.$rowPaymentSchedule['scheduleddate']. '</scheduleddate>';								
					  $xml = $xml.'<status>'.$rowPaymentSchedule['ExecApproval']. '</status>';								
					  $xml = $xml. '<monthlypayment>'.$rowPaymentSchedule['monthlypayment']. '</monthlypayment>';
					  $xml = $xml. '</paymentschedule>';
			   }
			   
			   // -- Default : xml & json values.
			   if($dataPaymentSchedules == null)
			   {
					  $xml = $xml.'<paymentschedule>';
					  $xml = $xml.'<applicationId>0</applicationId>';
					  $xml = $xml.'<item>0</item>';
					  $xml = $xml.'<scheduleddate>0000-00-00</scheduleddate>';								
					  $xml = $xml.'<status></status>';								
					  $xml = $xml. '<monthlypayment>0.00</monthlypayment>';
					  $xml = $xml. '</paymentschedule>';
			   }
		   
				$xml = $xml. '</paymentschedules>';
				
			// -- Determine the format.
			// -- Display JSON format.
			if($format == 'json')
			{
			   header('Content-type: application/json');	
			   $xmlString = simplexml_load_string($xml);
			   $json = json_encode($xmlString);
			    echo $json;
			   //$array = json_decode($json,TRUE);
			}
			// -- Display XML format. 
			// -- Default.
			else
			{
				header('Content-type: text/xml');
			    echo $xml;
			}	
// ----------------------------------------------------------------------------------- //
// 					END - IMS Amount to be paid and Still to be paid.				   //
// ----------------------------------------------------------------------------------- //				 
					   Database::disconnect();	
			}
			catch(Exception $e) 
			{
			  echo 'Message: ' .$e->getMessage();
			}
	
?>
