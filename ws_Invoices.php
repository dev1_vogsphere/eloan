<?php
/* require the user as the parameter 
customerid
ApplicationId
guid
repayment

*/
// -- Total Income
$tot_outstanding = 0.00;
$tot_paid = 0.00;
$tot_repayment = 0.00;
require 'database.php';

$customerid = null;
$ApplicationId = null;
$dataUserEmail = '';
$role = '';
$repaymentAmount = null;
$found = false;
					
// -- Repayment Amount.					
if ( !empty($_GET['repayment'])) 
					{
						$repaymentAmount = $_REQUEST['repayment'];
						// -- Decode Encrypt 17.09.2017 - Parameter Data.
						$repaymentAmount = base64_decode(urldecode($repaymentAmount)); 
						// -- Decode Encrypt 17.09.2017 - Parameter Data.
						$found = true;
					}
// -- customerid					
					if ( !empty($_GET['customerid'])) 
					{
						$customerid = $_REQUEST['customerid'];
						// -- Decode Encrypt 17.09.2017 - Parameter Data.
						$customerid = base64_decode(urldecode($customerid)); 
						// -- Decode Encrypt 17.09.2017 - Parameter Data.
						$found = true;
					}
// -- ApplicationId					
					if ( !empty($_GET['ApplicationId'])) 
					{
						$ApplicationId = $_REQUEST['ApplicationId'];
						// -- Decode Encrypt 17.09.2017 - Parameter Data.
						$ApplicationId = base64_decode(urldecode($ApplicationId)); 
						// -- Decode Encrypt 17.09.2017 - Parameter Data.
						$found = true;
					}					
					
// -- If all Parameters are populated. 
if($found) 
{

	/* soak in the passed variable or set our own */
	$format = 'xml';//strtolower($_GET['format']) == 'json' ? 'json' : 'xml'; //xml is the default
	
	// -- BOC 31.10.2017 --------- //
		 $pdo = Database::connect();
		 $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION); 
// ----------------------------------------------------------------------------------- //
// 					IMS Amount to be paid and Still to be paid.						   //
// ----------------------------------------------------------------------------------- //				 
		// -- Get Payment Invoices from common Tab Data.
		 $sql =  "select * from common where customer_identification = ?";
		 $q = $pdo->prepare($sql);
		 $q->execute(array($customerid));
		 $dataCommon = $q->fetchAll(PDO::FETCH_ASSOC);
					 
				$data = null;
				header('Content-type: text/xml');
				echo '<posts>';
				
			   // $dataCommon brings all the Invoices related to a client.
			   foreach($dataCommon as $rowCommon)
			   {
					 $invoice_id = $rowCommon['id'];
					 
 					 // -- Get Payment Details Data.
					  $sql =  "select * from payment where invoice_id = ? AND notes = ?";
					  $q = $pdo->prepare($sql);
					  $q->execute(array($invoice_id ,$ApplicationId));
					  $data = $q->fetchAll(PDO::FETCH_ASSOC);
					 
				// -- For each Invoice Get the Payments related to the Loan Application Id			 
					 foreach ($data as $row) 
					  {
						   		echo '<post>';
							   	echo '<invoice_id>'.$row['invoice_id']. '</invoice_id>';								
							    echo '<date>'.$row['date']. '</date>';
							   	echo '<amount>'.number_format($row['amount'],2). '</amount>';
							   	echo '</post>';
								$tot_paid = $tot_paid + $row['amount']; 
					   }
		  
			}	// End all Customer Invoices
			// -- Get the curreny 
			echo '</posts>';
// ----------------------------------------------------------------------------------- //
// 					END - IMS Amount to be paid and Still to be paid.				   //
// ----------------------------------------------------------------------------------- //				 
					   Database::disconnect();			
	// -- EOC 31.10.2017 -------- //
}?>
