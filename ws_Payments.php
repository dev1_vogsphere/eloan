<?php
/* require the user as the parameter 
customerid
ApplicationId
guid
repayment

PHP ODATA service Reference:
https://davidwalsh.name/web-service-php-mysql-xml-json
*/
// -- Total Income
$tot_outstanding = 0.00;
$tot_paid = 0.00;
$tot_repayment = 0.00;
require 'database_invoice2.php';

$customerid = null;
$ApplicationId = null;
$dataUserEmail = '';
$role = '';
$repaymentAmount = null;
$found = false;
$RemainingTerm = 0;
$paymentid = 0;		
$credit = 0.00;
$xml = "";
$paydate = ''; 
///////////////////////////////////////////////////////////////////
// -- BOC - Payment Loan Date Range Search Finacial Year 03.06.2021
// -- $Payment Range 
$mindate = '';
$maxdate  = '';
if ( !empty($_GET['mindate'])) 
{
		$mindate = $_REQUEST['mindate'];
		$mindate = base64_decode(urldecode($mindate)); 		
}
if ( !empty($_GET['maxdate'])) 
{
		$maxdate = $_REQUEST['maxdate'];
		$maxdate = base64_decode(urldecode($maxdate)); 		
}
// -- EOC - Payment Loan Date Range Search Finacial Year 03.06.2021
///////////////////////////////////////////////////////////////////
// -- $RemainingTerm
if ( !empty($_GET['RemainingTerm'])) 
{
		$RemainingTerm = $_REQUEST['RemainingTerm'];
		$found = true;
}
			
// -- Repayment Amount.					
if ( !empty($_GET['repayment'])) 
					{
						$repaymentAmount = $_REQUEST['repayment'];
						// -- Decode Encrypt 17.09.2017 - Parameter Data.
						$repaymentAmount = base64_decode(urldecode($repaymentAmount)); 
						// -- Decode Encrypt 17.09.2017 - Parameter Data.
						$found = true;
					}
// -- customerid					
					if ( !empty($_GET['customerid'])) 
					{
						$customerid = $_REQUEST['customerid'];
						// -- Decode Encrypt 17.09.2017 - Parameter Data.
						$customerid = base64_decode(urldecode($customerid)); 
						// -- Decode Encrypt 17.09.2017 - Parameter Data.
						$found = true;
					}
// -- ApplicationId					
					if ( !empty($_GET['ApplicationId'])) 
					{
						$ApplicationId = $_REQUEST['ApplicationId'];
						// -- Decode Encrypt 17.09.2017 - Parameter Data.
						$ApplicationId = base64_decode(urldecode($ApplicationId)); 
						// -- Decode Encrypt 17.09.2017 - Parameter Data.
						$found = true;
					}		
/* soak in the passed variable or set our own */					
$format = strtolower($_GET['format']) == 'json' ? 'json' : 'xml'; //xml is the default
					
// -- If all Parameters are populated. 
if($found) 
{
	/* soak in the passed variable or set our own */
	//$format = 'xml';//strtolower($_GET['format']) == 'json' ? 'json' : 'xml'; //xml is the default
	try
	{
	// -- BOC 31.10.2017 --------- //
		 $pdo = Database2::connect();
		 $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION); 
// ----------------------------------------------------------------------------------- //
// 					IMS Amount to be paid and Still to be paid.						   //
// ----------------------------------------------------------------------------------- //				 
		// -- Get Payment Invoices from common Tab Data.
		 $sql =  "select * from common where customer_identification = ?";
		 $q = $pdo->prepare($sql);
		 				// -- echo "<h2>Testing - $customerid</h2>";

		 $q->execute(array($customerid));
		 $dataCommon = $q->fetchAll(PDO::FETCH_ASSOC);
		 			 
					
			$dataInvoice = null;

			   $xml = '<invoices>';

			   $tot_outstanding = $repaymentAmount;		  
			   foreach($dataCommon as $rowCommon)
			   {
					 $invoice_id = $rowCommon['id'];
					 // -- BOC - Payment Loan Date Range Search Finacial Year 03.06.2021					 
					 if(empty($mindate))
					 {
 					   // -- Get Payment Details Data. No date Range Search
					   $sql =  "select * from payment where invoice_id = ? AND notes = ?";
					   $q = $pdo->prepare($sql);
					   $q->execute(array($invoice_id ,$ApplicationId));					   
					 }
					 else // -- payment Date Range Search
					 {
					   $sql =  "select * from payment where invoice_id = ? AND notes = ?
								AND date BETWEEN ? AND ?";	
					   $q = $pdo->prepare($sql);
					   $q->execute(array($invoice_id ,$ApplicationId,$mindate,$maxdate));										
					 }
					 // -- EOC - Payment Loan Date Range Search Finacial Year 03.06.2021					 					 
					 $dataInvoice = $q->fetchAll(PDO::FETCH_ASSOC);
					  
				// For each Invoice Get the Payments related to the Loan Application Id
					 foreach ($dataInvoice as $rowInvoice)
					  {
					  $paymentid = $rowInvoice['id'];
					  $iddocument = $rowInvoice['FILE'];
					  $path=str_replace(" ", '%20', $iddocument);
					  $tot_paid = $rowInvoice['amount'];
					  $tot_outstanding = $tot_outstanding - $tot_paid;
					  $xml = $xml.'<invoice>';
					  $xml = $xml.'<loanappid>'.$ApplicationId. '</loanappid>';
					  $xml = $xml.'<paymentid>'.$rowInvoice['id']. '</paymentid>';
					  $xml = $xml.'<paymentfile>'.$rowInvoice['FILE']. '</paymentfile>';								
					  $xml = $xml.'<invoiceid>'.$rowInvoice['invoice_id']. '</invoiceid>';								
					  $xml = $xml. '<date>'.$rowInvoice['date']. '</date>';
					  $xml = $xml. '<amount>'.number_format($rowInvoice['amount'],2). '</amount>';
					  $xml = $xml. '<outstandingamount>'.number_format($tot_outstanding,2). '</outstandingamount>';
					  $xml = $xml. '<paymentreference>Payment received - Thank you</paymentreference>';
					  $xml = $xml. '<currency>R</currency>';
					  $xml = $xml. '</invoice>';
								$RemainingTerm = $RemainingTerm - 1;  
								$credit = $credit + $tot_paid;
					   }
			      }	// End all Customer Invoices
				 if(!empty($paymentid))
				 {
						if($tot_outstanding == 0)
						{
							$RemainingTerm = 0;
						}
				 }
				 else
				 { 
						$tot_outstanding = $repaymentAmount;
						if($tot_outstanding == 0)
						{
							$RemainingTerm = 0;
						}
			     }
			
				// -- Remaining Term.
				if($RemainingTerm < 0)
				{
				  $RemainingTerm = 0;
				}
				$xml = $xml. '<totaldebt>'.number_format($repaymentAmount,2).'</totaldebt>';
				$xml = $xml. '<totalpaid>'.number_format($credit,2).'</totalpaid>';
				$xml = $xml. '<totaloutstanding>'.number_format($tot_outstanding,2).'</totaloutstanding>';
				$xml = $xml. '<RemainingTerm>'.$RemainingTerm.'</RemainingTerm>';
				$xml = $xml. '</invoices>';
				
			// -- Determine the format.
			
			// -- Display JSON format.
			if($format == 'json')
			{
			   header('Content-type: application/json');	
			   $xmlString = simplexml_load_string($xml);
			   $json = json_encode($xmlString);
			    echo $json;
			   //$array = json_decode($json,TRUE);
			}
			// -- Display XML format. 
			// -- Default.
			else
			{
				header('Content-type: text/xml');
			    echo $xml;
			}	
// ----------------------------------------------------------------------------------- //
// 					END - IMS Amount to be paid and Still to be paid.				   //
// ----------------------------------------------------------------------------------- //				 
					   Database2::disconnect();	
			}
			catch(Exception $e) 
			{
			  echo 'Message: ' .$e->getMessage();
			}
	
	}	
	// -- EOC 31.10.2017 -------- //
?>
